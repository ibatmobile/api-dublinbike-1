import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

import { Observable } from 'rxjs/Rx';
import { Movie } from '../model/movie.model';

@Injectable()
export class BikeService {
  API_ENDPOINT: string;
  API_KEY: string;
  API_URL: string;





  constructor(public http: Http) {
    this.API_ENDPOINT = "https://api.jcdecaux.com/vls/v1/stations?contract=dublin&apiKey=";
    this.API_KEY = "013c96fda1ac6937698c8402e42b0c31f2cc081e";
    this.API_URL = this.API_ENDPOINT + this.API_KEY

  }

  //https://scotch.io/tutorials/angular-2-http-requests-with-observables
  getListOfBikeStations(): Observable<any[]> {
    let serviceURL = this.API_URL;
    
    return this.http.get(serviceURL)
      // ...and calling .json() on the response to return data
      .map((res: Response) => res.json())
      //...errors if any
      .catch((error: any) => Observable.throw('Server error')

      );
  }


}
