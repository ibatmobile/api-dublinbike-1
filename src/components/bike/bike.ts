import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { BikeService } from '../../providers/bike'
@Component({
  selector: 'app-bike',
  templateUrl: 'app-bike.html'
})
export class Bike {

  text: string;
  listOfBikeStations: any;
  constructor(public bikeService: BikeService) {
    console.log('Hello Movie Component');
    this.text = 'Hello World';
    this.listOfBikeStations = [];
    this.bikeService.getListOfBikeStations().subscribe(listOfBikeStations => this.listOfBikeStations = listOfBikeStations);


  }

}
