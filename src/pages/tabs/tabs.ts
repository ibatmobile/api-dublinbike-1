import { Component } from '@angular/core';

import { BikePage } from '../bike/bike';

import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = BikePage;


  constructor() {

  }
}
