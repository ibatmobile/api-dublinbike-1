import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-bike',
  templateUrl: 'bike.html'
})
export class BikePage {

  constructor(public navCtrl: NavController) {

  }

}
