import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { Bike } from '../components/bike/bike';
import { BikePage } from '../pages/bike/bike';
import { BikeService } from '../providers/bike';

import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//@TODO - add the http module  - otherwise you will get error message: No provider for http 
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    MyApp,
    Bike,
    BikePage,
 
    HomePage,
    TabsPage
  ],
  imports: [
    BrowserModule,HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Bike,
    BikePage,
 
    HomePage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,BikeService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
